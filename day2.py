import math

###############################################################################

input_data = open('inputs/day2-1.txt').readlines()[0]
input_data = list(map(int, input_data.split(',')))


class IntCodeComputer(object):
    def __init__(self, memory):
        self.memory = memory

    def compute(self, p1, p2):
        memory = self.memory.copy()
        memory[1] = p1
        memory[2] = p2

        idx = 0
        opcode = memory[idx]

        while opcode != 99:
            val1 = memory[memory[idx + 1]]
            val2 = memory[memory[idx + 2]]
            if opcode == 1:
                memory[memory[idx + 3]] = val1 + val2
            elif opcode == 2:
                memory[memory[idx + 3]] = val1 * val2

            idx += 4
            opcode = memory[idx]

        return memory[0]


# Part 1 ######################################################################

int_cpu = IntCodeComputer(input_data)
res = int_cpu.compute(12, 2)
print(res)

# Part 2 ######################################################################

for noun in range(0, 100):
    for verb in range(0, 100):
        res = int_cpu.compute(noun, verb)
        if res == 19690720:
            print(100*noun + verb)
            exit()
