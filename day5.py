
###############################################################################

input_data = open('inputs/day5-1.txt').readlines()[0]
input_data = list(map(int, input_data.split(',')))

class IntCodeComputer(object):
    def __init__(self, memory):
        self.memory = memory
        self.modes = [False]*3  # 0 == position mode, 1 == immediate mode

    def set_mode(self, instruction):
        instruction = str(instruction).zfill(5)
        self.modes[0] = int(instruction[2]) == 1
        self.modes[1] = int(instruction[1]) == 1
        self.modes[2] = int(instruction[0]) == 1

    def compute(self, input_instr):
        memory = self.memory.copy()

        idx = 0
        while memory[idx] != 99:
            self.set_mode(memory[idx])
            if memory[idx] % 10 == 1:
                self.add(idx, memory)
                idx += 4
            elif memory[idx] % 10 == 2:
                self.multiply(idx, memory)
                idx += 4
            elif memory[idx] % 10 == 3:
                memory[memory[idx + 1]] = input_instr
                idx += 2
            elif memory[idx] % 10 == 4:
                if self.modes[0]:
                    print(memory[idx + 1])
                else:
                    print(memory[memory[idx + 1]])
                idx += 2
            elif memory[idx] % 10 == 5:
                idx = self.jump_if_true(idx, memory)
            elif memory[idx] % 10 == 6:
                idx = self.jump_if_false(idx, memory)
            elif memory[idx] % 10 == 7:
                self.less_than(idx, memory)
                idx += 4
            elif memory[idx] % 10 == 8:
                self.equals(idx, memory)
                idx += 4

    def add(self, idx, memory):
        if self.modes[0]:
            val1 = memory[idx + 1]
        else:
            val1 = memory[memory[idx + 1]]
        if self.modes[1]:
            val2 = memory[idx + 2]
        else:
            val2 = memory[memory[idx + 2]]
        memory[memory[idx + 3]] = val1 + val2

    def multiply(self, idx, memory):
        if self.modes[0]:
            val1 = memory[idx + 1]
        else:
            val1 = memory[memory[idx + 1]]
        if self.modes[1]:
            val2 = memory[idx + 2]
        else:
            val2 = memory[memory[idx + 2]]

        memory[memory[idx + 3]] = val1 * val2

    def jump_if_true(self, idx, memory):
        if self.modes[0]:
            val = memory[idx + 1]
        else:
            val = memory[memory[idx + 1]]
        if val != 0:
            if self.modes[1]:
                idx = memory[idx + 2]
            else:
                idx = memory[memory[idx + 2]]
            return idx
        return idx + 3

    def jump_if_false(self, idx, memory):
        if self.modes[0]:
            val = memory[idx + 1]
        else:
            val = memory[memory[idx + 1]]
        if val == 0:
            if self.modes[1]:
                idx = memory[idx + 2]
            else:
                idx = memory[memory[idx + 2]]
            return idx
        return idx + 3

    def less_than(self, idx, memory):
        if self.modes[0]:
            val1 = memory[idx + 1]
        else:
            val1 = memory[memory[idx + 1]]
        if self.modes[1]:
            val2 = memory[idx + 2]
        else:
            val2 = memory[memory[idx + 2]]

        if val1 < val2:
            memory[memory[idx + 3]] = 1
        else:
            memory[memory[idx + 3]] = 0

    def equals(self, idx, memory):
        if self.modes[0]:
            val1 = memory[idx + 1]
        else:
            val1 = memory[memory[idx + 1]]
        if self.modes[1]:
            val2 = memory[idx + 2]
        else:
            val2 = memory[memory[idx + 2]]

        if val1 == val2:
            memory[memory[idx + 3]] = 1
        else:
            memory[memory[idx + 3]] = 0

# Part 1 ######################################################################

int_cpu = IntCodeComputer(input_data)
int_cpu.compute(1)

print('Diagnostics finished.')

# Part 2 ######################################################################

int_cpu = IntCodeComputer(input_data)
int_cpu.compute(5)

print('Diagnostics finished.')
