import itertools

###############################################################################

input_data = open('inputs/day7-1.txt').readlines()[0]
input_data = list(map(int, input_data.split(',')))

class IntCodeComputer(object):
    def __init__(self, memory):
        self.memory = memory.copy()
        self.modes = [False]*3  # 0 == position mode, 1 == immediate mode
        self.idx = 0
        self.input_idx = 0

    def set_mode(self, instruction):
        instruction = str(instruction).zfill(5)
        self.modes[0] = int(instruction[2]) == 1
        self.modes[1] = int(instruction[1]) == 1
        self.modes[2] = int(instruction[0]) == 1

    def compute(self, input_instr):
        memory = self.memory

        while memory[self.idx] != 99:
            self.set_mode(memory[self.idx])
            if memory[self.idx] % 10 == 1:
                self.add(self.idx, memory)
                self.idx += 4
            elif memory[self.idx] % 10 == 2:
                self.multiply(self.idx, memory)
                self.idx += 4
            elif memory[self.idx] % 10 == 3:
                memory[memory[self.idx + 1]] = input_instr[self.input_idx]
                self.input_idx += 1
                self.idx += 2
            elif memory[self.idx] % 10 == 4:
                if self.modes[0]:
                    ret = memory[self.idx + 1]
                else:
                    ret = memory[memory[self.idx + 1]]
                self.idx += 2
                return ret
            elif memory[self.idx] % 10 == 5:
                self.idx = self.jump_if_true(self.idx, memory)
            elif memory[self.idx] % 10 == 6:
                self.idx = self.jump_if_false(self.idx, memory)
            elif memory[self.idx] % 10 == 7:
                self.less_than(self.idx, memory)
                self.idx += 4
            elif memory[self.idx] % 10 == 8:
                self.equals(self.idx, memory)
                self.idx += 4

        return 'S'

    def add(self, idx, memory):
        if self.modes[0]:
            val1 = memory[idx + 1]
        else:
            val1 = memory[memory[idx + 1]]
        if self.modes[1]:
            val2 = memory[idx + 2]
        else:
            val2 = memory[memory[idx + 2]]
        memory[memory[idx + 3]] = val1 + val2

    def multiply(self, idx, memory):
        if self.modes[0]:
            val1 = memory[idx + 1]
        else:
            val1 = memory[memory[idx + 1]]
        if self.modes[1]:
            val2 = memory[idx + 2]
        else:
            val2 = memory[memory[idx + 2]]

        memory[memory[idx + 3]] = val1 * val2

    def jump_if_true(self, idx, memory):
        if self.modes[0]:
            val = memory[idx + 1]
        else:
            val = memory[memory[idx + 1]]
        if val != 0:
            if self.modes[1]:
                idx = memory[idx + 2]
            else:
                idx = memory[memory[idx + 2]]
            return idx
        return idx + 3

    def jump_if_false(self, idx, memory):
        if self.modes[0]:
            val = memory[idx + 1]
        else:
            val = memory[memory[idx + 1]]
        if val == 0:
            if self.modes[1]:
                idx = memory[idx + 2]
            else:
                idx = memory[memory[idx + 2]]
            return idx
        return idx + 3

    def less_than(self, idx, memory):
        if self.modes[0]:
            val1 = memory[idx + 1]
        else:
            val1 = memory[memory[idx + 1]]
        if self.modes[1]:
            val2 = memory[idx + 2]
        else:
            val2 = memory[memory[idx + 2]]

        if val1 < val2:
            memory[memory[idx + 3]] = 1
        else:
            memory[memory[idx + 3]] = 0

    def equals(self, idx, memory):
        if self.modes[0]:
            val1 = memory[idx + 1]
        else:
            val1 = memory[memory[idx + 1]]
        if self.modes[1]:
            val2 = memory[idx + 2]
        else:
            val2 = memory[memory[idx + 2]]

        if val1 == val2:
            memory[memory[idx + 3]] = 1
        else:
            memory[memory[idx + 3]] = 0

# Part 1 ######################################################################

inputs = list(itertools.permutations([0, 1, 2, 3, 4]))
output_signal = []
for input_setting in inputs:
    init_val = 0
    for amp_idx in range(5):
        amp = IntCodeComputer(input_data)
        init_val = amp.compute([input_setting[amp_idx], init_val])
    output_signal.append(init_val)

print('The highest signal that can be sent is %d.' % max(output_signal))

# Part 2 ######################################################################

input_data = open('inputs/day7-1.txt').readlines()[0]
input_data = list(map(int, input_data.split(',')))

inputs = list(itertools.permutations([5, 6, 7, 8, 9]))
output_signal = []
for input_setting in inputs:
    amps = [IntCodeComputer(input_data), IntCodeComputer(input_data), \
            IntCodeComputer(input_data), IntCodeComputer(input_data), IntCodeComputer(input_data)]
    instruction_sets = [[inp] for inp in input_setting]
    init_val = 0
    while init_val != 'S':
        for amp_idx in range(5):
            instruction_sets[amp_idx].append(init_val)
            init_val = amps[amp_idx].compute(instruction_sets[amp_idx])
            if init_val == 'S':
                break
    output_signal.append(instruction_sets[0][-1])

print('The highest signal that can be sent is %d.' % max(output_signal))

