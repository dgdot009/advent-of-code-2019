import math
import numpy as np

###############################################################################

input_data = "168630-718098"
start, end = list(map(int, input_data.split('-')))

# Part 1 ######################################################################

cnt = 0
for pw in range(start, end+1):
    str_pw = str(pw)
    valid = False
    for i in range(1, 6):
        if str_pw[i] < str_pw[i-1]:
            valid = False
            break
        elif not valid and (str_pw[i] == str_pw[i-1]):
            valid = True
    if valid:
        cnt += 1

print('Number of passwords meeting the criteria: %d' % cnt)


# Part 2 ######################################################################

cnt = 0
for pw in range(start, end+1):
    str_pw = '_' + str(pw) + '_'
    valid = False
    for i in range(2, 7):
        if str_pw[i] < str_pw[i-1]:
            valid = False
            break
        elif not valid and ((str_pw[i] == str_pw[i-1]) and (str_pw[i+1] != str_pw[i]) and (str_pw[i-2] != str_pw[i])):
            valid = True
    if valid:
        cnt += 1

print('Number of passwords meeting the criteria: %d' % cnt)

