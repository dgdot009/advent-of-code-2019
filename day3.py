import math
import numpy as np

###############################################################################

input_data = open('inputs/day3-1.txt').readlines()
wire1_in = input_data[0].split(',')
wire2_in = input_data[1].split(',')

# Part 1 ######################################################################

UP = complex(0.0, 1.0)
DOWN = complex(0.0, -1.0)
LEFT = complex(-1.0, 0.0)
RIGHT = complex(1.0, 0.0)

def add_wire(wire, segment):
    """Add segments to the wire.

    Args:
        wire ([list]): The wire so far.
        segment ([string]): New segment
    """
    direction = segment[0]
    len_segment = int(segment[1:])
    if direction == 'R':
        for _ in range(len_segment):
            wire.append(wire[-1] + RIGHT)
    elif direction == 'L':
        for _ in range(len_segment):
                wire.append(wire[-1] + LEFT)
    elif direction == 'U':
        for _ in range(len_segment):
                wire.append(wire[-1] + UP)
    elif direction == 'D':
        for _ in range(len_segment):
            wire.append(wire[-1] + DOWN)

wire1 = [complex(0.0, 0.0)]
wire2 = [complex(0.0, 0.0)]

for segment in wire1_in:
    add_wire(wire1, segment)
for segment in wire2_in:
    add_wire(wire2, segment)

intersections = list(set(wire1) & set(wire2))[1:]
distances = [abs(c.real) + abs(c.imag) for c in intersections]
print('Minimum distance: ' + str(min(distances)))

# Part 2 ######################################################################

total_steps_to_intersections = [wire1.index(its) + wire2.index(its) for its in intersections]
print('Minimum combined steps required: ' + str(min(total_steps_to_intersections)))
