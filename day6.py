
###############################################################################

input_data = open('inputs/day6-1.txt').readlines()

class Orbit_Element(object):
    def __init__(self, name):
        self.name = name
        self.orbiting = []

    def add_orbiting(self, orbit_element):
        self.orbiting.append(orbit_element)

    def count_orbits(self, depth):
        sum_orbiting = depth
        for el in self.orbiting:
            sum_orbiting += el.count_orbits(depth + 1)
        return sum_orbiting

    def get_path_to(self, name, path_so_far):
        if self.name == name:
            return True

        for el in self.orbiting:
            if el.get_path_to(name, path_so_far):
                path_so_far.append(self.name)
                return True
        return False


# Part 1 ######################################################################

orbit_map = dict()

for orbit_descr in input_data:
    center, orbiting = orbit_descr.strip().split(')')
    if orbiting not in orbit_map:
        orbit_map[orbiting] = Orbit_Element(orbiting)
    if center not in orbit_map:
        orbit_map[center] = Orbit_Element(center)

    orbit_map[center].add_orbiting(orbit_map[orbiting])

orbit_cnt = orbit_map['COM'].count_orbits(0)
print('The total number of orbits is %d.' % orbit_cnt)

# Part 2 ######################################################################

path2you = []
orbit_map['COM'].get_path_to('YOU', path2you)
path2san = []
orbit_map['COM'].get_path_to('SAN', path2san)

unique = set(path2you) ^ set(path2san)
transfers = len(unique)

print('Minimum number of orbital transfers is %d' % transfers)
