import math

###############################################################################

input_data = open('inputs/day1-1.txt').readlines()


# Part 1 ######################################################################

sum_fuel = 0
for mass in input_data:
    sum_fuel += math.floor(int(mass)/3)-2

print(sum_fuel)

# Part 2 ######################################################################

sum_fuel = 0
for mass in input_data:
    added_fuel = math.floor(int(mass)/3)-2
    sum_fuel += added_fuel
    while True:
        added_fuel = math.floor(int(added_fuel)/3)-2
        if added_fuel <= 0:
            break
        sum_fuel += added_fuel

print(sum_fuel)